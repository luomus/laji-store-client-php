<?php
namespace LajiStoreClient\Http;

use Zend\Db\Adapter\Driver\ResultInterface;
use Zend\Http\Client;
use Zend\Http\Headers;
use Zend\Http\Request;
use Zend\Http\Response;
use Zend\Json\Json;

class Result implements ResultInterface
{
    const PAGE_PARAM = 'page';
    const PAGE_SIZE_PARAM = 'page_size';

    const MAX_RETRY = 2;

    const EMBEDDED_KEY = 'member';
    const TOTAL_KEY = 'totalItems';

    private $success = false;
    private $errorMessage;
    private $collection = false;
    private $total = 0;
    private $current_page = 0;
    private $current_item = 0;
    private $current_page_size = 0;

    protected $embeddedKey;

    protected $content;
    /** @var Request */
    protected $request;
    /** @var Client */
    protected $client;
    protected $response;

    public function initialize(Request $request, Client $client)
    {
        $this->embeddedKey = null;
        $this->request = $request;
        $this->client = $client;
        $this->makeRequest();
    }

    private function makeRequest($page = 1, $retryCnt = 0)
    {
        if ($this->current_page === $page) {
            return;
        }
        $this->setSuccess(false);
        $this->request->getQuery()->set(self::PAGE_PARAM, $page);
        $headers = new Headers();
        $headers->addHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json'
        ]);
        $this->request->setHeaders($headers);
        $this->response = $this->client->setUri($this->request->getUri()->toString())->send($this->request);
        if (!$this->response->isSuccess()) {
            if (self::MAX_RETRY > $retryCnt) {
                $retryCnt++;
                $this->makeRequest($page, $retryCnt);
                return;
            }
            $this->decodeBody($this->response->getStatusCode());
            $this->errorMessage = 'Connection to the server failed!';
            return;
        }
        if (!$this->decodeBody($this->response->getBody())) {
            return;
        }
        $this->setSuccess(true);
        $this->current_page = $page;
        $this->current_item = 0;
        $this->current_page_size = isset($this->content[self::PAGE_SIZE_PARAM]) ?
            $this->content[self::PAGE_SIZE_PARAM] : 0;
        $this->collection = isset($this->content[self::EMBEDDED_KEY]) ?
            $this->content[self::EMBEDDED_KEY] : false;
        $this->total = isset($this->content[self::TOTAL_KEY]) ?
            $this->content[self::TOTAL_KEY] : 0;
    }

    protected function decodeBody($content) {
        $this->content = $content;
        if (is_string($content)) {
            try {
                $this->content = Json::decode($content, Json::TYPE_ARRAY);
            } catch (\Exception $e) {
                $this->errorMessage = $e->getMessage();
                return false;
            }
        }
        return true;
    }

    /**
     * @return boolean
     */
    public function isSuccess()
    {
        return $this->success;
    }

    /**
     * @param boolean $success
     */
    public function setSuccess($success)
    {
        $this->success = $success;
    }

    /**
     * @return string
     */
    public function getErrorMessage()
    {
        return $this->errorMessage;
    }

    /**
     * @param string $errorMessage
     */
    public function setErrorMessage($errorMessage)
    {
        $this->errorMessage = $errorMessage;
    }

    /**
     * @return boolean
     */
    public function isCollection()
    {
        return $this->collection;
    }

    /**
     * @param boolean $collection
     */
    public function setCollection($collection)
    {
        $this->collection = $collection;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return mixed
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @param mixed $response
     */
    public function setResponse($response)
    {
        $this->response = $response;
    }

    /**
     * Return the current element
     * @link http://php.net/manual/en/iterator.current.php
     * @return mixed Can return any type.
     * @since 5.0.0
     */
    public function current()
    {
        return $this->content[self::EMBEDDED_KEY][$this->current_item];
    }

    /**
     * Move forward to next element
     * @link http://php.net/manual/en/iterator.next.php
     * @return void Any returned value is ignored.
     * @since 5.0.0
     */
    public function next()
    {
        $this->current_item++;
        if ($this->current_item >= $this->current_page_size) {
            $this->current_page++;
            $this->makeRequest($this->current_page);
        }
    }

    /**
     * Return the key of the current element
     * @link http://php.net/manual/en/iterator.key.php
     * @return mixed scalar on success, or null on failure.
     * @since 5.0.0
     */
    public function key()
    {
        return ($this->current_page - 1) * $this->current_page_size + $this->current_item;
    }

    /**
     * Checks if current position is valid
     * @link http://php.net/manual/en/iterator.valid.php
     * @return boolean The return value will be casted to boolean and then evaluated.
     * Returns true on success or false on failure.
     * @since 5.0.0
     */
    public function valid()
    {
        return $this->key() < $this->total;
    }

    /**
     * Rewind the Iterator to the first element
     * @link http://php.net/manual/en/iterator.rewind.php
     * @return void Any returned value is ignored.
     * @since 5.0.0
     */
    public function rewind()
    {
        $this->makeRequest(1);
    }

    /**
     * Force buffering
     *
     * @return void
     */
    public function buffer()
    {
        return;
    }

    /**
     * Check if is buffered
     *
     * @return bool|null
     */
    public function isBuffered()
    {
        return false;
    }

    /**
     * Is query result?
     *
     * @return bool
     */
    public function isQueryResult()
    {
        return $this->collection;
    }

    /**
     * Get affected rows
     *
     * @return int
     */
    public function getAffectedRows()
    {
        throw new \Exception('Not implemented');
    }

    /**
     * Get generated value
     *
     * @return mixed|null
     */
    public function getGeneratedValue()
    {
        throw new \Exception('Not implemented');
    }

    /**
     * Get the resource
     *
     * @return mixed
     */
    public function getResource()
    {
        throw new \Exception('Not implemented');
    }

    /**
     * Get field count
     *
     * @return int
     */
    public function getFieldCount()
    {
        throw new \Exception('Not implemented');
    }

    /**
     * Count elements of an object
     * @link http://php.net/manual/en/countable.count.php
     * @return int The custom count as an integer.
     * </p>
     * <p>
     * The return value is cast to an integer.
     * @since 5.1.0
     */
    public function count()
    {
        return isset($this->content[self::TOTAL_KEY]) ? $this->content[self::TOTAL_KEY] : (int)(!empty($this->content));
    }
}