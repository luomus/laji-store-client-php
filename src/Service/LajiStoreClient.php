<?php

namespace LajiStoreClient\Service;

use LajiStoreClient\Http\RequestBuilder;
use LajiStoreClient\Http\Result;
use Zend\Http\Client;

class LajiStoreClient
{
    const RESOURCE_FORMS = 'form';
    const RESOURCE_DOCUMENTS = 'document';
    const RESOURCE_DEVICE = 'device';
    const RESOURCE_INDIVIDUALS = 'individual';
    const RESOURCE_PING = 'ping';

    private $client;
    private $requestBuilder;

    public function __construct($baseUrl, Client $client = null)
    {
        $this->client = $client;
        if ($client === null) {
            $this->client = new Client();
        }
        $this->requestBuilder = new RequestBuilder($baseUrl);
    }

    public function sendGet($resource, $id = null, $params = [])
    {
        return $this->send('get', $resource, $id, $params);
    }

    public function sendPost($resource, $id = null, $params = [], $data = '')
    {
        return $this->send('post', $resource, $id, $params, $data);
    }

    public function sendPut($resource, $id = null, $params = [], $data = '') {
        return $this->send('put', $resource, $id, $params, $data);
    }

    public function sendDelete($resource, $id = null, $params = []) {
        return $this->send('delete', $resource, $id, $params);
    }

    private function send($method, $resource, $id = null, $params = [], $data = '') {
        $result = new Result();
        $result->initialize(
            $this->requestBuilder->{$method}($resource, $id, $params, $data),
            $this->client
        );
        return $result;
    }

}