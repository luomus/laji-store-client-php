<?php
namespace LajiStoreClientTest\Http;


use LajiStoreClient\Http\RequestBuilder;
use Zend\Http\Request;

class RequestBuilderTest extends \PHPUnit_Framework_TestCase
{
    protected $requestBuilder;

    public function testSettingBaseUrl()
    {
        $builder = new RequestBuilder('test');
        $this->assertEquals('test/foo',$builder->getUrl('foo'));
        $builder = new RequestBuilder('test/');
        $this->assertEquals('test/foo',$builder->getUrl('foo'));
    }

    public function testGetRequestBuilding()
    {
        $builder = new RequestBuilder('http://localhost/api');
        /** @var \Zend\Http\Request $request */
        $request = $builder->get('foo', 12);
        $this->assertInstanceOf('\Zend\Http\Request',$request);
        $this->assertEquals(Request::METHOD_GET, $request->getMethod());
        $this->assertEquals('http://localhost/api/foo/12', $request->getUri()->toString());

        $request = $builder->get('bar');
        $this->assertEquals(Request::METHOD_GET, $request->getMethod());
        $this->assertEquals('http://localhost/api/bar', $request->getUri()->toString());

        $request = $builder->get('bar', null, ['test' => 'true']);
        $this->assertEquals(Request::METHOD_GET, $request->getMethod());
        $this->assertEquals('http://localhost/api/bar', $request->getUri()->toString());
        $this->assertEquals('true', $request->getQuery('test'));
    }

    public function testAuthentication() {
        $builder = new RequestBuilder('http://viltsu:test@localhost/api');
        $request = $builder->get('foo', 3);
        $this->assertInstanceOf('\Zend\Http\Request',$request);
        $this->assertEquals(Request::METHOD_GET, $request->getMethod());
        $this->assertEquals('viltsu', $request->getUri()->getUser());
        $this->assertEquals('test', $request->getUri()->getPassword());
    }

    public function testPostRequestBuilding()
    {
        $builder = new RequestBuilder('http://localhost/api');
        /** @var \Zend\Http\Request $request */
        $request = $builder->post('form', 12);
        $this->assertInstanceOf('\Zend\Http\Request',$request);
        $this->assertEquals(Request::METHOD_POST, $request->getMethod());
        $this->assertEquals('http://localhost/api/form/12', $request->getUri()->toString());

        $request = $builder->post('haa', null, ['foo' => 'bar'], 'test=good');
        $this->assertInstanceOf('\Zend\Http\Request',$request);
        $this->assertEquals(Request::METHOD_POST, $request->getMethod());
        $this->assertEquals('http://localhost/api/haa', $request->getUri()->toString());
        $this->assertEquals('bar', $request->getQuery('foo'));
        $this->assertEquals('test=good', $request->getContent());
    }

    public function testPostRequestWithArrayBuilding()
    {
        $builder = new RequestBuilder('http://localhost/api');
        /** @var \Zend\Http\Request $request */
        $request = $builder->post('form', 12);
        $this->assertInstanceOf('\Zend\Http\Request',$request);
        $this->assertEquals(Request::METHOD_POST, $request->getMethod());
        $this->assertEquals('http://localhost/api/form/12', $request->getUri()->toString());

        $request = $builder->post('haa', null, ['foo' => 'bar'], ['test' => 'good']);
        $this->assertInstanceOf('\Zend\Http\Request',$request);
        $this->assertEquals(Request::METHOD_POST, $request->getMethod());
        $this->assertEquals('http://localhost/api/haa', $request->getUri()->toString());
        $this->assertEquals('bar', $request->getQuery('foo'));
        $this->assertEquals('{"test":"good"}', $request->getContent());
    }
}