<?php

namespace LajiStoreClient\Http;


use Zend\Http\Request;

class RequestBuilder
{
    CONST RESOURCE_FORM = 'forms';

    protected $request;
    protected $baseUrl;

    public function __construct($baseUrl)
    {
        $this->baseUrl = rtrim($baseUrl,'/');
    }

    public function getUrl($resource, $id = null) {
        if ($id == null) {
            return $this->baseUrl . '/' . $resource;
        }
        return $this->baseUrl . '/' . $resource . '/' . $id;
    }

    public function get($resource, $id = null, $params = []) {
        return $this->getRequest(Request::METHOD_GET, $resource, $id, $params);
    }

    public function post($resource, $id = null, $params = [], $data = '') {
        return $this->getRequest(Request::METHOD_POST, $resource, $id, $params, $data);
    }

    public function put($resource, $id = null, $params = [], $data = '') {
        return $this->getRequest(Request::METHOD_PUT, $resource, $id, $params, $data);
    }

    public function delete($resource, $id, $params = []) {
        return $this->getRequest(Request::METHOD_DELETE, $resource, $id, $params);
    }

    protected function getRequest($method, $resource, $id = null, $params = [], $data = '') {
        $request = new Request();
        $request->setMethod($method);
        $request->setUri($this->getUrl($resource, $id));
        if (!empty($params)) {
            $request->getQuery()->fromArray($params);
        }
        if (!empty($data)) {
            if (is_string($data)) {
                $request->setContent($data);
            } else if (is_array($data)) {
                $request->setContent(json_encode($data));
            } else {
                throw new \Exception('Invalid data given to ' . __CLASS__);
            }
        }

        return $request;
    }
}